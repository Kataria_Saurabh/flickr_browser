package com.example.saurabhkataria.flickr_browser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Saurabh Kataria on 28-01-2016.
 */
public class RecyclerviewAdapter extends RecyclerView.Adapter<ImageView_Holder> {

    private List<Photo> mPhotos;
    private Context mContext;
    private final String Log_Tag = RecyclerviewAdapter.class.getSimpleName();

    public RecyclerviewAdapter(Context mcontext, List<Photo> mphotos) {
        this.mContext = mcontext;
        this.mPhotos = mphotos;
    }

    @Override
    public ImageView_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browser, null);
        return new ImageView_Holder(view);
        }

    @Override
    public void onBindViewHolder(ImageView_Holder holder, int position) {

        Photo aPhotoItem = mPhotos.get(position);
        Log.d(Log_Tag, "Processing: " + aPhotoItem.getmTitle() + " --> " + Integer.toString(position));

        Picasso.with(mContext).load(aPhotoItem.getmPhotoUrl())
                .error(R.drawable.default_placeholder)
                .placeholder(R.drawable.default_placeholder)
                .into(holder.ivThumbNail);
        holder.tvTitle.setText(aPhotoItem.getmTitle());

    }

    public void loadNewData(List<Photo> newphotos ){
        mPhotos = newphotos;
        notifyDataSetChanged();
    }

    public Photo getphoto(int position){
        return ( null != mPhotos ? mPhotos.get(position): null) ;

    }

    @Override
    public int getItemCount() {
        return ( mPhotos != null ? mPhotos.size() : 0);
    }
}

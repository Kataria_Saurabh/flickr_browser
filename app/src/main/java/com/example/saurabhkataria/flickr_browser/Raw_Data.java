package com.example.saurabhkataria.flickr_browser;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Saurabh Kataria on 27-01-2016.
 */
public class Raw_Data {
    enum Download_Status {Idle, Not_Initialized, Successful , Processing, Failed_Or_Empty};
    private String Log_Tag= Raw_Data.class.getSimpleName();
    private String mRawData;
    private String mData;
    private Download_Status mDownloadStatus;

    public Raw_Data(String mRawData) {
        this.mRawData = mRawData;
        this.mDownloadStatus= Download_Status.Idle;
    }

    public void Reset(){
        this.mDownloadStatus=Download_Status.Idle;
        this.mData= null;
        this.mRawData = null;
    }

    public void execute(){
        this.mDownloadStatus=Download_Status.Processing;
        Download_RawData aDownload_RawdataObj = new Download_RawData();
        aDownload_RawdataObj.execute(mRawData);
    }

    public void setmRawData(String s) {
        this.mRawData = mRawData;
    }

    public String getmData() {
        return mData;
    }

    public Download_Status getmDownloadStatus() {

        return mDownloadStatus;
    }

    public class Download_RawData extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection aHttpConnection = null;
            BufferedReader aBufferedReader = null;
            StringBuilder aStrBuffer = new StringBuilder();
            if(params == null)
                return null;
            try{
                URL aUrl = new URL(params[0]);
                aHttpConnection = (HttpURLConnection) aUrl.openConnection();
                aHttpConnection.setRequestMethod("GET");
                aHttpConnection.connect();

                InputStream aIs = aHttpConnection.getInputStream();
                if(aIs == null)
                    return null ;


                aBufferedReader = new BufferedReader(new InputStreamReader(aIs));

                String aLine = aBufferedReader.readLine();
                while(aLine != null){
                   aStrBuffer.append(aLine + "\n");
                   // Log.d("Error", "Spmething is wrong" + strbuffer);
                }
                return aStrBuffer.toString();
            }catch (IOException e){
                Log.e(Log_Tag,"Error Caught in Input", e);
                return null;
            }finally {
                if(aHttpConnection !=null)
                    aHttpConnection.disconnect();

                if(aBufferedReader !=null){
                    try {
                        aBufferedReader.close();
                    } catch (IOException e) {
                        Log.e(Log_Tag, "Error in Closing the Reader", e );
                        e.printStackTrace();
                    }
                            }}

            }

        @Override
        protected void onPostExecute(String Web_Data) {
            mData = Web_Data;
            Log.v(Log_Tag, "Data returned was: " +mData);
            if (mData == null){
                if(mRawData == null){
                    mDownloadStatus = Download_Status.Not_Initialized;
                }
                else
                    mDownloadStatus = Download_Status.Failed_Or_Empty;
            }
            else
                mDownloadStatus= Download_Status.Successful;
        }
    }



}

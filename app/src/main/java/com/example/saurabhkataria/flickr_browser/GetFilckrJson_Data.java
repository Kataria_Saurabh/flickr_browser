package com.example.saurabhkataria.flickr_browser;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Saurabh Kataria on 27-01-2016.*/


public class GetFilckrJson_Data extends Raw_Data{

    private String Log_Tag = GetFilckrJson_Data.class.getSimpleName();
    private ArrayList<Photo> mPhotoArrayList;
    private Uri mUri;

    public GetFilckrJson_Data(String searchTag, boolean matchAll) {
        super(null) ;
        createAndUpdateUri(searchTag, matchAll);
        mPhotoArrayList = new ArrayList<Photo>();
    }

    public void execute (){
        super.setmRawData(mUri.toString());
        Download_Json_Data object = new Download_Json_Data();
        Log.v(Log_Tag, "Uri Built" + mUri.toString());
        object.execute(mUri.toString());
    }

    public ArrayList<Photo> getmPhotoArrayList() {
        return mPhotoArrayList;
    }

    public boolean createAndUpdateUri(String searchTag, boolean matchAll) {
        final String aBaseUri= "https://api.flickr.com/services/feeds/photos_public.gne";
        final String aParamTags ="tags";
        final String aParamTagsMode ="tagmode";
        final String aParamFormat ="format";
        final String aParamNoJsonCallback ="nojsoncallback";

        mUri = Uri.parse(aBaseUri).buildUpon().appendQueryParameter(aParamTags,searchTag)
                .appendQueryParameter(aParamTagsMode,matchAll ? "All" : "Any")
                .appendQueryParameter(aParamFormat, "json")
                .appendQueryParameter(aParamNoJsonCallback, "1")
                .build();

        return (mUri != null);

    }
    public void processResult(){
        if(getmDownloadStatus() != Download_Status.Successful){
            Log.e(Log_Tag, "Error in  Downloading Raw File");
        }
        // TODO: 30-01-2016 if happens then break :  
        final String Flicker_Items = "items";
        final String Flicker_Title = "tvTitle";
       // final String Flicker_Link= "link";
        final String Flicker_Author="author";
        final String Flicker_AuthorId= "author_id";
        final String Flicker_Media="media";
        final String Flicker_Tags="tags";
        final String Flicker_Photo_Url="m";
        try {
            JSONObject aJsonObj = new JSONObject(getmData());
            JSONArray aArrayObj = aJsonObj.getJSONArray(Flicker_Items);
            for (int i=0; i< aArrayObj.length(); i++){
                JSONObject aPhotoObj = aArrayObj.getJSONObject(i);
                String aTitle = aPhotoObj.getString(Flicker_Title);

                String aAuthor = aPhotoObj.getString(Flicker_Author);
                String aAuthorId = aPhotoObj.getString(Flicker_AuthorId);
                String aTags = aPhotoObj.getString(Flicker_Tags);

                JSONObject aMedia = aPhotoObj.getJSONObject(Flicker_Media);
                String aPhotoUrl = aMedia.getString(Flicker_Photo_Url);

                String aLink = aPhotoUrl.replaceFirst("_m.", "_b.");

                Photo aPhotoObject = new Photo(aTitle, aAuthor, aAuthorId, aLink, aTags, aPhotoUrl);
                mPhotoArrayList.add(aPhotoObject);
            }
            for (Photo singlephoto : mPhotoArrayList){
                Log.d("OUTPUT", singlephoto.toString());
            }

        }catch (JSONException jsone){
            Log.e(Log_Tag , "Error occured while JSON Parsing" + jsone);
            jsone.printStackTrace();
        }

    }

    public class Download_Json_Data extends Download_RawData {
        @Override
        protected String doInBackground(String... params) {
            String [] aPar = {mUri.toString()};
            return super.doInBackground(aPar);
        }



        @Override
        protected void onPostExecute(String Web_Data) {
            super.onPostExecute(Web_Data);
            processResult();
        }
    }
}

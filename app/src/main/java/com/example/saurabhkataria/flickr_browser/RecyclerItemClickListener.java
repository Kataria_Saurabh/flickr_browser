package com.example.saurabhkataria.flickr_browser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Saurabh Kataria on 01-02-2016.
 */

public class RecyclerItemClickListener implements RecyclerView.OnTouchListener {

    public static interface OnItemClickListener{
        public void OnItemClick(View view, int position);
        public void OnItemLongClick(View view, int position);
    }

    private OnItemClickListener mListener;
    private GestureDetector mGestureDetector;

    public RecyclerItemClickListener(MainActivity Listener, final RecyclerView context, final OnItemClickListener recyclerview) {
        mListener = (OnItemClickListener) Listener;
        mGestureDetector = new GestureDetector(Listener, new GestureDetector.SimpleOnGestureListener(){
            public boolean onSingleTapUp(MotionEvent e){
                return true;
            }
            public void onLongPress(MotionEvent e){
                View childview = context.findChildViewUnder(e.getX(),e.getY());
                if( childview !=null && mListener != null) {
                    mListener.OnItemLongClick(childview, context.getChildPosition(childview));
                }
            }
        }
                );
    }

    public boolean onInterceptTouchEvent (RecyclerView view, MotionEvent e){
        View childview = view.findChildViewUnder(e.getX(),e.getY());
        if( childview !=null && mListener != null){
            mListener.OnItemLongClick(childview, view.getChildPosition(childview));
        }
        return false;
    }
/*public void onTouchEvent (RecyclerView view, MotionEvent e) {
    }*/


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}

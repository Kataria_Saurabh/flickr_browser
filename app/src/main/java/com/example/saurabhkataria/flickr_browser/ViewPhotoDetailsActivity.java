package com.example.saurabhkataria.flickr_browser;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ViewPhotoDetailsActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);

       activateToolbarWithHomeEnabled();
        Intent intent = getIntent();
        Photo aPhotoObject = (Photo) intent.getSerializableExtra(mPhoto_Transfer);

        TextView tvPhotoTitle = (TextView) findViewById(R.id.photo_title);
        tvPhotoTitle.setText("Title" + aPhotoObject.getmTitle());

        TextView tvPhotoTag = (TextView) findViewById(R.id.photo_tag);
        tvPhotoTag.setText("Title" + aPhotoObject.getmTag());

        TextView tvPhotoAuthor = (TextView) findViewById(R.id.photo_author);
        tvPhotoAuthor.setText("Title" + aPhotoObject.getmAuthor());

        ImageView ivPhotoImage = (ImageView) findViewById(R.id.photoimage);
        Picasso.with(this).load(aPhotoObject.getmLink())
                .error(R.drawable.default_placeholder)
                .placeholder(R.drawable.default_placeholder)
                .into(ivPhotoImage);

    }



}

package com.example.saurabhkataria.flickr_browser;

import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

public class Search_Activity extends BaseActivity {
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        activateToolbarWithHomeEnabled();

       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
    }
    protected boolean onCreateoptionsMenu( Menu menu){
        getMenuInflater().inflate(R.menu.search_menu, menu);
        final MenuItem aSearchItem = menu.findItem(R.id.action_settings);
        mSearchView =(SearchView) aSearchItem.getActionView();
        SearchManager searchmanager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchmanager.getSearchableInfo(getComponentName()));
        mSearchView.setIconified(false);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SharedPreferences aSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                aSharedPref.edit().putString(mFlickr_Query, query).commit();
                mSearchView.clearFocus();
                finish();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_settings){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.saurabhkataria.flickr_browser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private static final String Log_Tag ="MainActivity";
    private List<Photo> mPhotoList = new ArrayList<Photo>();
    private RecyclerView mRecyclerView;
    private RecyclerviewAdapter mRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Raw_Data raw_data = new Raw_Data("https://api.flickr.com/services/feeds/photos_public.gne?tags=google,monkey,car&format=json&nojsoncallback=1");
        raw_data.execute();
         /*GetFilckrJson_Data jsondataflickr = new GetFilckrJson_Data("Batman, Monster",true);
        jsondataflickr.execute();*/

       /*setToolbar();

        mRecyclerView = (RecyclerView) findViewById(R.id.Recycler_View);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerViewAdapter = new RecyclerviewAdapter(MainActivity.this, new ArrayList<Photo>());
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        mRecyclerView.addOnItemTouchListener((RecyclerView.OnItemTouchListener) new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                Toast.makeText(MainActivity.this, "Normal Tap", Toast.LENGTH_SHORT);
            }

            @Override
            public void OnItemLongClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, ViewPhotoDetailsActivity.class);
                intent.putExtra(mPhoto_Transfer, mRecyclerViewAdapter.getphoto(position));
                startActivity(intent);
            }

        }));*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        String aQuery = getSavedPreferenceData(mFlickr_Query);
        if(aQuery.length()>0){
            Process_Photos aProcessPhotos = new Process_Photos(aQuery, true);
            aProcessPhotos.execute();
        }
    }
    private String getSavedPreferenceData(String key){
        SharedPreferences aSharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return aSharedPref.getString("key","");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.menu_search){
            Intent intent = new Intent(this, Search_Activity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class Process_Photos extends GetFilckrJson_Data {

        public Process_Photos(String searchTag, boolean matchAll) {
            super(searchTag, matchAll);
        }
        public void execute (){
        //    super.execute();
            Processing_Photos aProcessing_Photos = new Processing_Photos();
            aProcessing_Photos.execute();
        }
        public class Processing_Photos extends Download_Json_Data {
            @Override
            protected void onPostExecute(String Web_Data) {
                super.onPostExecute(Web_Data);
                mRecyclerViewAdapter.loadNewData(getmPhotoArrayList());
            }
        }
    }
}

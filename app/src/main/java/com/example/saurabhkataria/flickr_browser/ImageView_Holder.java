package com.example.saurabhkataria.flickr_browser;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Saurabh Kataria on 28-01-2016.
 */
public class ImageView_Holder extends RecyclerView.ViewHolder {
    protected ImageView ivThumbNail;
    protected TextView tvTitle;
    public ImageView_Holder(View itemView) {
        super(itemView);
        this.ivThumbNail = (ImageView) itemView.findViewById(R.id.imglist);
        this.tvTitle = (TextView) itemView.findViewById(R.id.txtlist);
    }
}

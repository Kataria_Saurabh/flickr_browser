package com.example.saurabhkataria.flickr_browser;



import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;



/**
 * Created by Saurabh Kataria on 28-01-2016.
 */
public class BaseActivity extends AppCompatActivity
{
    private Toolbar mToolbar;
    public final String mFlickr_Query = "mFlickr_Query";
    public final String mPhoto_Transfer = "Photo Transfer";

    protected Toolbar setToolbar (){
        if(mToolbar == null)
        {
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            if(mToolbar != null)
            {
                setSupportActionBar(mToolbar);
            }
        }
        return mToolbar;
    }
    protected Toolbar activateToolbarWithHomeEnabled(){
        setToolbar();
        if(mToolbar !=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        return mToolbar;
    }
}
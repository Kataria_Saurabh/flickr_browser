package com.example.saurabhkataria.flickr_browser;

import java.io.Serializable;

/**
 * Created by Saurabh Kataria on 27-01-2016.
 */
public class Photo implements Serializable {
    private static final long mSerial_Version_Uid = 1L;
    private String mTitle;
    private String mAuthor;
    private String mAuthoriId;
    private String mLink;
    private String mTag;
    private String mPhotoUrl;

    public Photo(String mTitle, String mAuthor, String mAuthoriId, String mLink, String mTag, String mphotourl) {
        this.mTitle = mTitle;
        this.mAuthor = mAuthor;
        this.mAuthoriId = mAuthoriId;
        this.mLink = mLink;
        this.mTag = mTag;
        this.mPhotoUrl = mphotourl;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmAuthor() {
        return mAuthor;
    }

    public String getmAuthoriId() {
        return mAuthoriId;
    }

    public String getmLink() {
        return mLink;
    }

    public String getmTag() {
        return mTag;
    }

    public String getmPhotoUrl() {
        return mPhotoUrl;
    }

    public String toString() {
        return "Photo{" +
                "tvTitle='" + mTitle + '\'' +
                ", mAuthor='" + mAuthor + '\'' +
                ", mAuthorId='" + mAuthoriId + '\'' +
                ", mLink='" + mLink + '\'' +
                ", mTags='" + mTag + '\'' +
                ", mImage='" + mPhotoUrl + '\'' +
                '}';
    }
}
